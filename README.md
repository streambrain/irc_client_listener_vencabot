# irc_client_listener_vencabot
## Description
This module extends the streambrain_vencabot package with a 'Listener' which spawns Events based on a few popular IRC client events.

## Status
As of right now, it creates appropriate Events when the connected client receives a PRIVMSG, PING, JOIN, USERSTATE, or NOTICE message.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
